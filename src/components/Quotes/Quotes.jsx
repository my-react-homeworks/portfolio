import { useTranslation } from 'react-i18next';

import {quots} from './quots';

function Quotes(){

    const {t} = useTranslation();

    const randomQuote = Math.floor(Math.random() * 16);

    return (
        <div className="quotes">
            <div className="quote">{t(`${quots[randomQuote].quote}`)}</div>
            <div className="author">- {t(`${quots[randomQuote].author}`)}</div>
        </div>
    );
}

export default Quotes;