export const quots = [
    {
        quote: "quotes.q1.quote",
        author: "quotes.q1.author",
    },
    {
        quote: "quotes.q2.quote",
        author: "quotes.q2.author",
    },
    {
        quote: "quotes.q3.quote",
        author: "quotes.q3.author",
    },
    {
        quote: "quotes.q4.quote",
        author: "quotes.q4.author",
    },
    {
        quote: "quotes.q5.quote",
        author: "quotes.q5.author",
    },
    {
        quote: "quotes.q6.quote",
        author: "quotes.q6.author",
    },
    {
        quote: "quotes.q7.quote",
        author: "quotes.q7.author",
    },
    {
        quote: "quotes.q8.quote",
        author: "quotes.q8.author",
    },
    {
        quote: "quotes.q9.quote",
        author: "quotes.q9.author",
    },
    {
        quote: "quotes.q10.quote",
        author: "quotes.q10.author",
    },
    {
        quote: "quotes.q11.quote",
        author: "quotes.q11.author",
    },
    {
        quote: "quotes.q12.quote",
        author: "quotes.q12.author",
    },
    {
        quote: "quotes.q13.quote",
        author: "quotes.q13.author",
    },
    {
        quote: "quotes.q14.quote",
        author: "quotes.q14.author",
    },
    {
        quote: "quotes.q15.quote",
        author: "quotes.q15.author",
    },
    {
        quote: "quotes.q16.quote",
        author: "quotes.q16.author",
    }
];
