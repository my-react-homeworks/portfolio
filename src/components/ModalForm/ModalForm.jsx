import { useState } from "react";
import { useForm } from "react-hook-form";
import { useTranslation } from "react-i18next";

import Button from "../Button";

function ModalForm({ close }) {

    const {t} = useTranslation();

    const [sendStatus, setSendStatus] = useState(null);

    const {
        register,
        handleSubmit,
        formState: { errors, isValid },
    } = useForm({
        mode: "onBlur"
    });

    function click(e) {
        if (e.target === e.currentTarget) {
            close();
        }
    }

    function formHandler(data) {
        fetch(
            "https://api.telegram.org/bot6625083493:AAG7Zf_o_lN-RZJ4jbvXU_WTCtniabDan9c/sendMessage",
            {
                method: "POST",
                headers: {
                    "Content-Type": "application/json",
                },
                body: JSON.stringify({
                    chat_id: 1076815618,
                    parse_mode: "HTML",
                    text: `<strong><i>New message from portfolio</i></strong>\n\n<strong>Name:</strong> ${data.name}\n<strong>Email:</strong> <a href="mailto://${data.email}">${data.email}</a>\n<strong>Subject:</strong> ${data.subject}\n<strong>Message:</strong>\n${data.message}`,
                }),
            }
        )
            .then((res) => {
                return res.json();
            })
            .then((data) => {
                if (data.ok) {
                    setSendStatus("success");
                } else {
                    setSendStatus("error");
                }
            })
            .catch((error) => {
                console.warn(error);
            });
    }

    return (
        <div className="overlay" onClick={(e) => click(e)}>
            {!sendStatus && (
                <div className="modal">
                    <div className="modal-title">{t("form.title")}</div>
                    <form
                        autoComplete="off"
                        onSubmit={handleSubmit(formHandler)}
                    >
                        <div>
                            <div>
                                <input
                                    type="text"
                                    placeholder={t("form.name")}
                                    {...register("name", {
                                        required: {
                                            value: true,
                                            message: `${t(
                                                "form.errors.required"
                                            )}`,
                                        },
                                        minLength: {
                                            value: 4,
                                            message: `${t(
                                                "form.errors.name.min"
                                            )}`,
                                        },
                                        maxLength: {
                                            value: 15,
                                            message: `${t(
                                                "form.errors.name.max"
                                            )}`,
                                        },
                                        pattern: {
                                            value: /^[a-zа-я]+$/i,
                                            message: `${t(
                                                "form.errors.name.pattern"
                                            )}`,
                                        },
                                    })}
                                />
                                {errors.name && (
                                    <div className="field-error">
                                        {errors.name.message}
                                    </div>
                                )}
                            </div>
                            <div>
                                <input
                                    type="text"
                                    placeholder={t("form.email")}
                                    {...register("email", {
                                        required: {
                                            value: true,
                                            message: `${t(
                                                "form.errors.required"
                                            )}`,
                                        },
                                        pattern: {
                                            value: /[a-z0-9.-_]+@[a-z0-9_.-]+\.[a-z]{2,10}/,
                                            message: `${t(
                                                "form.errors.email"
                                            )}`,
                                        },
                                    })}
                                />
                                {errors.email && (
                                    <div className="field-error">
                                        {errors.email.message}
                                    </div>
                                )}
                            </div>
                        </div>
                        <div>
                            <div>
                                <input
                                    type="text"
                                    placeholder={t("form.subject")}
                                    {...register("subject", {
                                        required: {
                                            value: true,
                                            message: `${t(
                                                "form.errors.required"
                                            )}`,
                                        },
                                        minLength: {
                                            value: 5,
                                            message: `${t(
                                                "form.errors.subject.min"
                                            )}`,
                                        },
                                        maxLength: {
                                            value: 35,
                                            message: `${t(
                                                "form.errors.subject.max"
                                            )}`,
                                        },
                                        pattern: {
                                            value: /^[a-zа-я]+$/i,
                                            message: `${t(
                                                "form.errors.subject.pattern"
                                            )}`,
                                        },
                                    })}
                                />
                                {errors.subject && (
                                    <div className="field-error">
                                        {errors.subject.message}
                                    </div>
                                )}
                            </div>
                        </div>
                        <div>
                            <div>
                                <textarea
                                    placeholder={t("form.message")}
                                    rows={5}
                                    {...register("message", {
                                        required: {
                                            value: true,
                                            message: `${t(
                                                "form.errors.required"
                                            )}`,
                                        },
                                        minLength: {
                                            value: 5,
                                            message: `${t(
                                                "form.errors.message.min"
                                            )}`,
                                        },
                                        maxLength: {
                                            value: 250,
                                            message: `${t(
                                                "form.errors.message.max"
                                            )}`,
                                        },
                                    })}
                                />
                                {errors.message && (
                                    <div className="field-error">
                                        {errors.message.message}
                                    </div>
                                )}
                            </div>
                        </div>
                        <div>
                            <Button
                                text={t("form.button")}
                                disabled={!isValid}
                            />
                        </div>
                    </form>
                    <div className="close" onClick={(e) => click(e)}>
                        {t("form.close")} ✘
                    </div>
                </div>
            )}
            {sendStatus === "success" && (
                <div className="message-status success">
                    <div className="icon">✔</div>
                    <div className="text">{t("form.status.success")}</div>
                    <Button text={t("form.status.button")} click={close} />
                </div>
            )}
            {sendStatus === "error" && (
                <div className="message-status error">
                    <div className="icon">!</div>
                    <div className="text">{t("form.status.error")}</div>
                    <Button text={t("form.status.button")} click={close} />
                </div>
            )}
        </div>
    );
}

export default ModalForm;
