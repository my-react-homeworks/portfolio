import { Link } from "react-router-dom";

import logo from "../../images/LogoFull.svg";

function Logo(){
    return (
        <div className="logo">
            <Link to="/">
                <img src={logo} alt="Logo" />
                <div>Anton Biletskyi</div>
            </Link>
        </div>
    );
}

export default Logo;