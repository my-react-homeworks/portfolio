function PageTitle({title}){
    return (
        <div className="page-title">
            <span className="prefix">/</span>
            <div>{title}</div>
        </div>
    );
}

export default PageTitle;