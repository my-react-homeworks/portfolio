import { NavLink } from 'react-router-dom';
import { useTranslation } from "react-i18next";

import Logo from '../Logo';
import SocIcons from '../SocIcons';
import closeIcon from '../../images/closeMobileIcon.svg';

function MobileMenu({close, langswitch}){

    const {t} = useTranslation();

    const lang = localStorage.getItem("i18nextLng");

    return(
        <div className="mobile-menu-container">
            <div className="mobile-menu-head">
                <Logo />
                <img className="close-menu" src={closeIcon} alt="Close" onClick={close}/>
            </div>
            <nav>
                <ul className='menu' onClick={close}>
                    <li><NavLink to="/"><span className="prefix">#</span>{t("header.menu.home")}</NavLink></li>
                        <li><NavLink to="works"><span className="prefix">#</span>{t("header.menu.works")}</NavLink></li>
                        <li><NavLink to="about"><span className="prefix">#</span>{t("header.menu.about")}</NavLink></li>
                        <li><NavLink to="contacts"><span className="prefix">#</span>{t("header.menu.contacts")}</NavLink></li>
                </ul>
                <select name="langswitcher" onChange={(e) => {langswitch(e.target.value)}} defaultValue={lang}>
                    <option value="en">EN</option>
                    <option value="uk">UA</option>
                </select>
            </nav>
            <div className="mobile-menu-footer">
                <SocIcons />
            </div>
        </div>
    )
}

export default MobileMenu;