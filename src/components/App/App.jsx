import Header from '../Header';
import Main from '../Main';
import Footer from '../Footer';
import TopSocials from '../TopSocials';

function App(){
    return(
        <div className="wrapper">
            <TopSocials />
            <Header />
            <Main />
            <Footer />
        </div>
    )
}

export default App;