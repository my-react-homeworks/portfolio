import { Routes, Route } from "react-router-dom";

import Homepage from "../../pages/Homepage";
import Workspage from "../../pages/Workspage";
import Aboutpage from "../../pages/Aboutpage";
import Contactspage from "../../pages/Contactspage";

function Main() {
    return (
        <main>
            <div className="container">
                <Routes>
                    <Route path="/" element={<Homepage />} />
                    <Route path="works" element={<Workspage />} />
                    <Route path="about" element={<Aboutpage />} />
                    <Route path="contacts" element={<Contactspage />} />
                </Routes>
            </div>
        </main>
    );
}

export default Main;
