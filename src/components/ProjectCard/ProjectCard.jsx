import { useTranslation } from 'react-i18next';

import Button from '../Button';

function ProjectCard({info}){

    const {t} = useTranslation();

    function buttonClick(){
        if(info.link){
            window.open(info.link);
        }        
    }

    return(
        <div className="project-card">
            {info.preview && <div className="project-preview">
                <img src={info.preview} alt={`${info.name} preview`} />
            </div>}
            <div className="project-tech">{info.tech.join(" ")}</div>
            <div className="project-info">
                <div className="project-name">{t(`${info.name}`)}</div>
                <div className="project-desc">{t(`${info.desc}`)}</div>
                <Button text={t("projects.button")} icon="<~>" click={buttonClick} />
            </div>
        </div>
    )
}

export default ProjectCard;