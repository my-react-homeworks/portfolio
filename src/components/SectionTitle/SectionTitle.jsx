import { Link } from "react-router-dom";

function SectionTitle({title, hr, more}){
    return (
        <div className="section-title">
            <div className="section-title-head">
                <span className="prefix">#</span>
                <div>{title}</div>
                {hr && <hr style={{ maxWidth: `${hr}px` }} />}
            </div>
            {more && (
                <div className="more">
                    <Link to="works">
                        {more}
                        <span>&nbsp;{"~~>"}</span>
                    </Link>
                </div>
            )}
        </div>
    );
}

export default SectionTitle;