import { Link } from "react-router-dom";

import gitIcon from "../../images/Github.svg";
import linkedinIcon from "../../images/Linkedin.svg";
import discordIcon from "../../images/Discord.svg";

function SocIcons() {
    return (
        <>
            <div className="social-icon">
                <Link to="https://github.com/toha-web" target="blank">
                    <img src={gitIcon} alt="Git" />
                </Link>
            </div>
            <div className="social-icon">
                <Link
                    to="https://linkedin.com/in/anton-biletskyi"
                    target="blank"
                >
                    <img src={linkedinIcon} alt="Linkedin" />
                </Link>
            </div>
            <div className="social-icon">
                <Link to="https://discordapp.com/users/_.toha._" target="blank">
                    <img src={discordIcon} alt="Discord" />
                </Link>
            </div>
        </>
    );
}

export default SocIcons;
