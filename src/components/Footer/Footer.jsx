import { Link } from "react-router-dom";

import Logo from "../Logo";
import SocIcons from '../SocIcons'

function Footer (){
    return (
        <footer>
            <div className="container">
                <div className="info">
                    <div className="info-logo">
                        <Logo />
                        <div>Front-end developer</div>
                    </div>
                    <div className="info-socials">
                        <SocIcons />
                    </div>
                </div>
                <div className="copyright">
                    <span>&#169; Copyright {new Date().getFullYear()}.</span>
                    <span>Development by&nbsp;<Link to="mailto:t.info@ukr.net">Anton Biletskyi</Link></span>
                </div>
            </div>
        </footer>
    );
}

export default Footer;