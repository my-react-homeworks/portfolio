import { NavLink } from 'react-router-dom';
import { useState } from 'react';
import i18n from "../../i18n";
import { useTranslation } from 'react-i18next';

import Logo from '../Logo';
import MobileMenu from '../MobileMenu';

import mobileIcon from '../../images/mobileIcon.svg';

function Header(){

    const [isMobile, setIsMobile] = useState(false);
    const lang = localStorage.getItem("i18nextLng");

    const {t} = useTranslation();

    function showMobileMenu(){
        setIsMobile(true);
        document.body.classList.add("freeze");
    }

    function closeMobileMenu() {
        setIsMobile(false);
        document.body.classList.remove("freeze");
    }

    function switchLanguage(value){
        i18n.changeLanguage(value);
    }

    return (
        <header>
            <div className='container'>
                <Logo />
                <nav>
                    <ul className='menu'>
                        <li><NavLink to="/"><span className="prefix">#</span>{t("header.menu.home")}</NavLink></li>
                        <li><NavLink to="works"><span className="prefix">#</span>{t("header.menu.works")}</NavLink></li>
                        <li><NavLink to="about"><span className="prefix">#</span>{t("header.menu.about")}</NavLink></li>
                        <li><NavLink to="contacts"><span className="prefix">#</span>{t("header.menu.contacts")}</NavLink></li>
                    </ul>
                    <select name="langswitcher" onChange={(e) => {switchLanguage(e.target.value)}} defaultValue={lang}>
                        <option value="en">EN</option>
                        <option value="uk">UA</option>
                    </select>
                </nav>
                {!isMobile && <img className="mobile-icon" src={mobileIcon} alt="Mobile Menu" onClick={showMobileMenu}/>}
                {isMobile && <MobileMenu close={closeMobileMenu} langswitch={switchLanguage}/>}
            </div>
        </header>
    );
}

export default Header;