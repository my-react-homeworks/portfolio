function SkillCard({title, skills}){
    return (
        <div className="skill-card">
            <div className="skill-card-title">{title}</div>
            <div className="skill-card-body">{skills}</div>
        </div>
    )
}

export default SkillCard;