import SocIcons from "../SocIcons";

function TopSocials(){
    return (
        <div className="socials">
            <div className="line"></div>
            <SocIcons />
        </div>
    );
}

export default TopSocials;