function Button({ text, icon, click, disabled }) {
    return (
        <button
            onClick={(e) => {
                if(!click) return;
                click(e);
            }}
            disabled={disabled}
        >
            {text}
            {icon && <span>&nbsp;{icon}</span>}
        </button>
    );
}

export default Button;
