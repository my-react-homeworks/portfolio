import i18n from "i18next";
import { initReactI18next } from "react-i18next";

// import Backend from "i18next-http-backend";
import LanguageDetector from "i18next-browser-languagedetector";

import translationEN from './data/translationEN.json';
import translationUK from './data/translationUK.json';

i18n
    // .use(Backend)
    .use(LanguageDetector)
    .use(initReactI18next)
    .init({
        supportedLngs: ["en", "uk"],
        fallbackLng: "en",
        debug: false,
        resources: {
            en: {
                translation: translationEN
            },
            uk: {
                translation: translationUK
            }
        }
    });

export default i18n;
