import { useTranslation } from 'react-i18next';

import PageTitle from '../../components/PageTitle';

import telegram from '../../images/Telegram.svg';
import email from "../../images/Email.svg";
import linkedin from "../../images/Linkedin.svg";
import discord from "../../images/Discord.svg";
import landmark from '../../images/Landmark.svg';

function Contactspage(){

    const {t} = useTranslation();

    window.scrollTo(0, 0);

    return (
        <div className="contacts-page page">
            <PageTitle title={t("contacts.pagetitle")} />
            <section>
                <div className="intro">
                    <p>{t("contacts.p1")}</p>
                    <p>{t("contacts.p2")}</p>
                </div>
                <div className="info">
                    <div className="info-title">
                        {t("contacts.table-title")}
                    </div>
                    <div className="info-body">
                        <div>
                            <img src={landmark} alt="Location" />
                            {t("contacts.adress")}
                        </div>
                        <div>
                            <img src={telegram} alt="Telegram" />
                            <a href="http://t.me/II_toha_II" target="_blank">
                                @II_toha_II
                            </a>
                        </div>
                        <div>
                            <img src={discord} alt="Discord" />
                            <a
                                href="https://discordapp.com/users/_.toha._"
                                target="_blank"
                            >
                                _.toha._
                            </a>
                        </div>
                        <div>
                            <img src={email} alt="Email" />
                            <a href="mailto:t.info@ukr.net">t.info@ukr.net</a>
                        </div>
                        <div>
                            <img src={linkedin} alt="Linkedin" />
                            <a
                                href="https://linkedin.com/in/anton-biletskyi"
                                target="_blank"
                            >
                                Anton Biletskyi
                            </a>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    );
}

export default Contactspage;