import Welcome from './layouts/Welcome';
import Quotes from '../../components/Quotes';
import Projects from './layouts/Projects';
import Skills from './layouts/Skills';
import About from './layouts/About';
import Contacts from './layouts/Contacts';

function Homepage(){

    window.scrollTo(0, 0);

    return(
        <div className='homepage page'>
            <Welcome />
            <Quotes />
            <Projects />
            <Skills />
            <About />
            <Contacts />
        </div>
    )
}

export default Homepage;