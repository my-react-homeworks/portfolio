import { useTranslation } from 'react-i18next';

import SectionTitle from '../../../components/SectionTitle';

import telegram from '../../../images/Telegram.svg';
import email from '../../../images/Email.svg';

function Contacts(){

    const {t} = useTranslation();

    return (
        <section>
            <SectionTitle title={t("homepage.contacts.title")} hr={125} />
            <div className="contacts">
                <div className="contacts-intro">
                    <p>{t("homepage.contacts.text")}</p>
                </div>
                <div className="contacts-table">
                    <div className="contacts-table-title">{t("homepage.contacts.table-title")}</div>
                    <div className="contacts-table-body">
                        <div>
                            <img src={telegram} alt="Telegram" />
                            <a href="http://t.me/II_toha_II" target="_blank">
                                @II_toha_II
                            </a>
                        </div>
                        <div>
                            <img src={email} alt="Email" />
                            <a href="mailto:t.info@ukr.net">t.info@ukr.net</a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    );
}

export default Contacts;