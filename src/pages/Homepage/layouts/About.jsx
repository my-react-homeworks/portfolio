import { useNavigate } from "react-router-dom";
import { useTranslation } from "react-i18next";

import SectionTitle from "../../../components/SectionTitle";
import Button from "../../../components/Button";

import avatar from '../../../images/about-avatar.png';
import dots from '../../../images/dots.svg';

function About() {

    const navigate = useNavigate();

    const {t} = useTranslation();

    function buttonClick(){
        navigate("/about");
    }

    return (
        <section>
            <SectionTitle title={t("homepage.about.title")} hr={325} />
            <div className="about">
                <div className="about-text">
                    <p>{t("homepage.about.p1")}</p>
                    <p>{t("homepage.about.p2")}</p>
                    <p>{t("homepage.about.p3")}</p>
                    <Button
                        text={t("homepage.about.button")}
                        icon="~>"
                        click={buttonClick}
                    />
                </div>
                <div className="about-image">
                    <img src={avatar} alt="Avatar" />
                    <div className="about-image-dots">
                        <img src={dots} alt="Dots" />
                    </div>
                </div>
            </div>
        </section>
    );
}

export default About;
