import { useTranslation } from "react-i18next";

import SectionTitle from "../../../components/SectionTitle";
import ProjectCard from "../../../components/ProjectCard";

import {projectsBase} from "../../../data/projectsBase";
const projects = [
    projectsBase.complete.find((proj) => proj.id === 5),
    projectsBase.small.find((proj) => proj.id === 7),
    projectsBase.complete.find((proj) => proj.id === 4),
];

function Projects() {

    const {t} = useTranslation();

    return (
        <section>
            <SectionTitle title={t("homepage.projects.title")} hr={500} more={t("homepage.projects.more")} />
            <div className="projects">
                {projects.map(proj => {
                    return <ProjectCard key={proj.id} info={proj} />;
                })}
            </div>
        </section>
    );
}

export default Projects;
