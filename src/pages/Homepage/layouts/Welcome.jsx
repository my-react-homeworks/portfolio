import { useState } from 'react';
import { useTranslation, Trans } from "react-i18next";

import avatar from '../../../images/first-avatar.png';
import dots from '../../../images/dots.svg';
import Button from '../../../components/Button';
import Modal from '../../../components/ModalForm';


function Welcome() {

    const {t} = useTranslation();

    const [modalStatus, setModalStatus] = useState(false);

    function modalOpen(){
        setModalStatus(true);
        document.body.classList.add("freeze");
    }
    function modalClose(){
        setModalStatus(false);
        document.body.classList.remove("freeze");
    }

    return (
        <section>
            <div className="welcome">
                <div className="intro">
                    <h1>
                        <Trans i18nKey="homepage.welcome.title">
                            <span>Hello</span>, my name is <span>Anton and </span>i am <span>a front-end</span>developer
                        </Trans>
                    </h1>
                    <p>{t("homepage.welcome.text")}</p>
                    <Button
                        text={t("homepage.welcome.button")}
                        click={modalOpen}
                    />
                    {modalStatus && <Modal close={modalClose} />}
                </div>
                <div className="avatar">
                    <div className="avatar-image">
                        <img src={avatar} alt="Avatar" />
                        <div className="dots">
                            <img src={dots} alt="Dots" />
                        </div>
                    </div>
                    <div className="position">
                        <div className="position-text">
                            <Trans i18nKey="homepage.welcome.position">
                                Currently working: <a href="https://pogodnik.com" target='blank'>Portfolio</a>
                            </Trans>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    );
}

export default Welcome;
