import { useTranslation } from "react-i18next";

import SectionTitle from "../../../components/SectionTitle";
import SkillCard from '../../../components/SkillCard';

function Skills(){

    const {t} = useTranslation();

    return (
        <section>
            <SectionTitle title={t("homepage.skills.title")} hr={240} />
            <div className="skills">
                <SkillCard title={t("homepage.skills.languages")} skills="HTML CSS JavaScript TypeScript" />
                <SkillCard title={t("homepage.skills.frameworks")} skills="Next.js" />
                <SkillCard title={t("homepage.skills.libraries")} skills="React Redux SCSS MUI jQuery Bootstrap" />
                <SkillCard title={t("homepage.skills.tools")} skills="VSCode Git Figma" />
            </div>
        </section>
    );
}

export default Skills;