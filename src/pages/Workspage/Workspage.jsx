import { useTranslation } from "react-i18next";

import PageTitle from "../../components/PageTitle";
import SectionTitle from '../../components/SectionTitle';
import ProjectCard from '../../components/ProjectCard';

import {projectsBase} from '../../data/projectsBase';
const fullProjects = projectsBase.complete.reverse();
const smallProjects = projectsBase.small.reverse();
const testTasks = projectsBase.test.reverse();

function Workspage(){

    const {t} = useTranslation();

    window.scrollTo(0, 0);

    return (
        <div className="myprojects page">
            <PageTitle title={t("works.pagetitle")} />
            <section>
                <SectionTitle title={t("works.complete")} />
                <div className="projects">
                    {fullProjects.map((proj) => {
                        return <ProjectCard key={proj.id} info={proj} />;
                    })}
                </div>
            </section>
            <section>
                <SectionTitle title={t("works.small")} />
                <div className="projects">
                    {smallProjects.map((proj) => {
                        return <ProjectCard key={proj.id} info={proj} />;
                    })}
                </div>
            </section>
            <section>
                <SectionTitle title={t("works.test")} />
                <div className="projects">
                    {testTasks.map((proj) => {
                        return <ProjectCard key={proj.id} info={proj} />;
                    })}
                </div>
            </section>
        </div>
    );
}

export default Workspage;