import { useTranslation } from 'react-i18next';

import PageTitle from '../../components/PageTitle';
import SectionTitle from '../../components/SectionTitle';
import SkillCard from '../../components/SkillCard';
import Button from '../../components/Button';

import avatar from '../../images/about-avatar.png';
import dots from '../../images/dots.svg';

function Aboutpage(){

    const {t} = useTranslation();

    window.scrollTo(0, 0);

    function buttonClick(){
        window.open(`${t("about.resume.link")}`);
    }

    return (
        <div className="about-me page">
            <PageTitle title={t("about.pagetitle")} />
            <section>
                <div className="about">
                    <div className="about-text">
                        <p>{t("about.p1")}</p>
                        <p>{t("about.p2")}</p>
                        <p>{t("about.p3")}</p>
                        <p>{t("about.p4")}</p>
                        <p>{t("about.p5")}</p>
                    </div>
                    <div className="about-image">
                        <img src={avatar} alt="Avatar" />
                        <div className="about-image-dots">
                            <img src={dots} alt="Dots" />
                        </div>
                    </div>
                </div>
            </section>
            <section>
                <SectionTitle title={t("homepage.skills.title")} />
                <div className="skills">
                    <SkillCard
                        title={t("homepage.skills.languages")}
                        skills="HTML CSS JavaScript TypeScript"
                    />
                    <SkillCard
                        title={t("homepage.skills.frameworks")}
                        skills="Next.js"
                    />
                    <SkillCard
                        title={t("homepage.skills.libraries")}
                        skills="React Redux SCSS MUI jQuery Bootstrap"
                    />
                    <SkillCard
                        title={t("homepage.skills.tools")}
                        skills="VSCode Git Figma"
                    />
                    <SkillCard
                        title={t("about.softskills.title")}
                        skills={t("about.softskills.skills")}
                    />
                </div>
            </section>
            <section>
                <SectionTitle title={t("about.resume.title")} />
                <p>{t("about.resume.text")}</p>
                <Button text={t("about.resume.button")} click={buttonClick} />
            </section>
        </div>
    );
}

export default Aboutpage;